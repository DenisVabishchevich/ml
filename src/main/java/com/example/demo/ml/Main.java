package com.example.demo.ml;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomTree;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;

import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        String filePath = "src/main/java/com/example/demo/ml/123.arff";
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(filePath);
        Instances data = source.getDataSet();
//        data.setClassIndex(1);

        ReplaceMissingValues missingValues = new ReplaceMissingValues();
        missingValues.setInputFormat(data);
        Instances removedMissing = Filter.useFilter(data, missingValues);

        Remove remove = new Remove();
        remove.setAttributeIndices("1,4,7,8,9,11,12");
        remove.setInputFormat(removedMissing);
        Instances removedColumns = Filter.useFilter(removedMissing, remove);

        Discretize discretize = new Discretize();
        discretize.setOptions(new String[]{"-R", "first-last", "-O"});
        discretize.setInputFormat(removedColumns);
        Instances descData = Filter.useFilter(removedColumns, discretize);

        descData.setClassIndex(0);
        //initialize the info gain extractor
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        Ranker search = new Ranker();

        AttributeSelection attSelect = new AttributeSelection();
        attSelect.setEvaluator(eval);
        attSelect.setSearch(search);
        attSelect.SelectAttributes(descData);

        //let's show the information gain value for each attribute
        System.out.println(attSelect.toResultsString());



        RandomTree tree = new RandomTree();
        tree.buildClassifier(descData);

        System.out.println(tree);

        ArffSaver saver = new ArffSaver();
        saver.setInstances(descData);
        saver.setFile(new File("src/main/java/com/example/demo/ml/123Mod.arff"));
        saver.writeBatch();


    }
}
